package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2020/utils"
)

type grid map[point]bool

func main() {
	// test outcome: 112
	lines, _ := utils.ImportToStringSlice("input.txt")
	g := make(grid)

	// Init initial space
	for i, line := range lines {
		for j, c := range line {
			if string(c) == "#" {
				g[point{i, j, 0, 0}] = true
			}
		}
	}

	// Problem 1
	for i := 0; i <= 6; i++ {
		fmt.Printf("Step %d: Active nodes: %d\n", i, len(g))
		g = g.step()
	}
}

type point struct {
	X, Y, Z, W int
}

/*
If a cube is active and exactly 2 or 3 of its neighbors are also active, the cube remains active. Otherwise, the cube becomes inactive.
If a cube is inactive but exactly 3 of its neighbors are active, the cube becomes active. Otherwise, the cube remains inactive.
*/
func (g grid) step() grid {
	newgrid := make(grid)
	for k := range g {
		// Already active points
		n := g.numNeighbours(k)
		if n == 2 || n == 3 {
			newgrid[k] = true
		}
		// Not yet active points (unoptimized, we search around every point)
		neighbours := g.adjecentPoints(k)
		for _, nb := range neighbours {
			if g.numNeighbours(nb) == 3 {
				newgrid[nb] = true
			}
		}
	}
	return newgrid
}

func (g grid) numNeighbours(p point) int {
	aps := g.adjecentPoints(p)
	sum := 0
	for _, ap := range aps {
		if _, ok := g[ap]; ok {
			sum++
		}
	}
	return sum
}

func (g grid) adjecentPoints(p point) []point {
	search := []point{
		{-1, -1, -1, -1}, {0, -1, -1, -1}, {1, -1, -1, -1},
		{-1, 0, -1, -1}, {0, 0, -1, -1}, {1, 0, -1, -1},
		{-1, 1, -1, -1}, {0, 1, -1, -1}, {1, 1, -1, -1},

		{-1, -1, 0, -1}, {0, -1, 0, -1}, {1, -1, 0, -1},
		{-1, 0, 0, -1}, {0, 0, 0, -1}, {1, 0, 0, -1},
		{-1, 1, 0, -1}, {0, 1, 0, -1}, {1, 1, 0, -1},

		{-1, -1, 1, -1}, {0, -1, 1, -1}, {1, -1, 1, -1},
		{-1, 0, 1, -1}, {0, 0, 1, -1}, {1, 0, 1, -1},
		{-1, 1, 1, -1}, {0, 1, 1, -1}, {1, 1, 1, -1},

		{-1, -1, -1, 0}, {0, -1, -1, 0}, {1, -1, -1, 0},
		{-1, 0, -1, 0}, {0, 0, -1, 0}, {1, 0, -1, 0},
		{-1, 1, -1, 0}, {0, 1, -1, 0}, {1, 1, -1, 0},

		{-1, -1, 0, 0}, {0, -1, 0, 0}, {1, -1, 0, 0},
		{-1, 0, 0, 0}, {1, 0, 0, 0},
		{-1, 1, 0, 0}, {0, 1, 0, 0}, {1, 1, 0, 0},

		{-1, -1, 1, 0}, {0, -1, 1, 0}, {1, -1, 1, 0},
		{-1, 0, 1, 0}, {0, 0, 1, 0}, {1, 0, 1, 0},
		{-1, 1, 1, 0}, {0, 1, 1, 0}, {1, 1, 1, 0},

		{-1, -1, -1, 1}, {0, -1, -1, 1}, {1, -1, -1, 1},
		{-1, 0, -1, 1}, {0, 0, -1, 1}, {1, 0, -1, 1},
		{-1, 1, -1, 1}, {0, 1, -1, 1}, {1, 1, -1, 1},

		{-1, -1, 0, 1}, {0, -1, 0, 1}, {1, -1, 0, 1},
		{-1, 0, 0, 1}, {0, 0, 0, 1}, {1, 0, 0, 1},
		{-1, 1, 0, 1}, {0, 1, 0, 1}, {1, 1, 0, 1},

		{-1, -1, 1, 1}, {0, -1, 1, 1}, {1, -1, 1, 1},
		{-1, 0, 1, 1}, {0, 0, 1, 1}, {1, 0, 1, 1},
		{-1, 1, 1, 1}, {0, 1, 1, 1}, {1, 1, 1, 1},
	}
	points := []point{}
	for _, s := range search {
		points = append(points, point{p.X + s.X, p.Y + s.Y, p.Z + s.Z, p.W + s.W})
	}
	return points
}
