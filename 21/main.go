package main

import (
	"fmt"
	"regexp"
	"sort"
	"strings"

	set "github.com/deckarep/golang-set"
	"gitlab.com/hectormalot/aoc2020/utils"
)

func main() {
	lines, _ := utils.ImportToStringSlice("input.txt")

	A2I := make(map[string]set.Set)
	I2A := make(map[string]set.Set)
	allingredients := []string{}

	regex := regexp.MustCompile(`^([^(]+)\(contains ((?:\w+,? ?)+)\)$`)

	for _, line := range lines {
		res := regex.FindStringSubmatch(line)
		ing := strings.Split(strings.TrimSpace(res[1]), " ")
		all := strings.Split(strings.ReplaceAll(res[2], ",", ""), " ")

		// Add to allergen2ingredient list
		for _, a := range all {
			s := set.NewSet()
			for _, i := range ing {
				s.Add(i)
			}
			if v, ok := A2I[a]; ok {
				A2I[a] = v.Intersect(s)
				continue
			}
			A2I[a] = s
		}

		// Populate the ingredient2allergen list
		for _, i := range ing {
			allingredients = append(allingredients, i)

			s := set.NewSet()
			for _, a := range all {
				s.Add(a)
			}
			if v, ok := I2A[i]; ok {
				I2A[i] = v.Union(s)
				continue
			}
			I2A[i] = s
		}
	}

	// Solve known links
	answer := make(map[string]string)
	for i := 0; i < len(A2I); i++ {
		for k, v := range A2I {
			if v.Cardinality() == 1 {
				ing := v.ToSlice()[0].(string)
				answer[k] = ing

				// Remove this match from other sets
				for _, v2 := range A2I {
					v2.Remove(ing)
				}
			}
		}
	}

	// Make list of all ingredients
	ingredients := set.NewSet()
	for i := range I2A {
		ingredients.Add(i)
	}

	// Remove known links to allergens
	for _, v := range answer {
		ingredients.Remove(v)
	}

	// count occurences
	sum := 0
	for _, ing := range allingredients {
		if ingredients.Contains(ing) {
			sum++
		}
	}
	fmt.Println("Problem 1:", sum, "\n\n ")

	matches := []match{}
	for k, v := range answer {
		matches = append(matches, match{i: v, a: k})
	}

	sort.Slice(matches, func(i, j int) bool {
		return matches[i].a < matches[j].a
	})

	result := []string{}
	for _, m := range matches {
		fmt.Println(m.a, m.i)
		result = append(result, m.i)
	}
	fmt.Println("Problem 2:", strings.Join(result, ","))
}

type match struct {
	i, a string
}
