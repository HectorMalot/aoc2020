package main

import (
	"fmt"
	"image/color"
	"log"
	"math"
	"strings"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"

	"gitlab.com/hectormalot/aoc2020/utils"
)

type point struct {
	x, y int
}

// Tile is a 10x10 grid of # and .
// edges are represented LtR and TtB
// they can be flipped
type Tile struct {
	ID         int
	raw        []string
	edges      []string
	neighbours map[int]*Tile
	pos        point
	allocated  bool
}

func main() {
	lines, _ := utils.ImportToStringSlice("input.txt")

	// Parse
	var tile *Tile
	tiles := make(map[int]*Tile)
	for _, line := range lines {
		if line == "" {
			// wrap up current tile
			tile.initEdges()
			tiles[tile.ID] = tile
			continue
		}
		if string(line[0]) == "T" {
			// start new tile
			tile = new(Tile)
			tile.neighbours = make(map[int]*Tile)
			fmt.Sscanf(line, "Tile %d:", &tile.ID)
			continue
		}
		tile.raw = append(tile.raw, line)
	}

	// Find # of neighbours for each tile
	for k1, t1 := range tiles {
		for k2, t2 := range tiles {
			if k1 == k2 {
				continue // skip self
			}
			m, _ := t1.match(t2)
			if m >= 0 {
				// t1.neighbours = append(t1.neighbours, k2)
				t1.neighbours[k2] = t2
				// t2.neighbours = append(t2.neighbours, k1)
				t2.neighbours[k1] = t1
			}
		}
	}

	// Result problem 1
	res := 1
	for k, t := range tiles {
		if len(t.neighbours) == 2 {
			res *= k
		}
	}
	fmt.Println("Problem 1:", res) // 111936085519519

	// Solve the actual puzzle (instead of the cornere pieces)
	// - pick 1 tile, assign to 0,0
	// - neighbour by neighbour, add the other tiles to the grid

	// Create 1 big grid
	grid := make(map[point]*Tile)

	// Get a first tile and allocate to 0,0
	var currentTile *Tile
	for _, t := range tiles {
		currentTile = t
		t.pos = point{0, 0}
		grid[t.pos] = t
		currentTile.allocated = true
		break
	}

	// Solve puzzle
	allocateTiles(currentTile, grid)

	// Create 1 grid with borders removed
	// -- Shift so that we can create a huge array
	xshift, yshift := 0, 0
	for k := range grid {
		if k.x < xshift {
			xshift = k.x
		}
		if k.y < yshift {
			yshift = k.y
		}
	}

	newGrid := make(map[point]*Tile)
	for k, v := range grid {
		v.pos = point{k.x - xshift, k.y - yshift}
		newGrid[v.pos] = v
	}
	grid = newGrid

	// testgrid := make(map[point]*Tile)
	// testgrid[point{0, 0}] = grid[point{0, 0}]
	// testgrid[point{0, 1}] = grid[point{0, 1}]
	// testgrid[point{0, 2}] = grid[point{0, 2}]
	// testgrid[point{1, 0}] = grid[point{1, 0}]
	// testgrid[point{1, 1}] = grid[point{1, 1}]
	// testgrid[point{1, 2}] = grid[point{1, 2}]
	// testgrid[point{2, 0}] = grid[point{2, 0}]
	// testgrid[point{2, 1}] = grid[point{2, 1}]
	// testgrid[point{2, 2}] = grid[point{2, 2}]

	// grid = testgrid

	// grid[point{0, 0}].print()
	// grid[point{0, 1}].print()
	// grid[point{0, 2}].print()
	// grid[point{1, 0}].print()
	// grid[point{1, 1}].print()
	// grid[point{1, 2}].print()

	// Create 1 big array
	length := int(math.Sqrt(float64(len(grid))))
	tilesize := len(grid[point{0, 0}].raw[0]) - 2

	searchGrid := []string{}
	for y := 0; y < length*(tilesize+2); y++ {
		row := ""
		for x := 0; x < length; x++ {
			// for x := 0; x < length*tilesize; x += tilesize {
			ry := y % (tilesize + 2)
			ty := y / (tilesize + 2)
			t := grid[point{x, ty}]
			row += t.raw[9-ry][1 : 1+tilesize]
		}
		// searchGrid = append([]string{row}, searchGrid...)
		searchGrid = append(searchGrid, row)
	}

	// Oops, remove every 0th and 10th line please
	tmpGrid := []string{}
	for i, line := range searchGrid {
		// fmt.Print("\n", line)
		if i%10 == 0 || i%10 == 9 {
			// fmt.Print(" << exit <<")
			continue
		}
		tmpGrid = append(tmpGrid, line)
	}
	searchGrid = tmpGrid

	// Search for the sea monster

	monster := []string{
		"                  # ",
		"#    ##    ##    ###",
		" #  #  #  #  #  #   ",
	}
	// testmonster := []string{
	// 	"                   # ",
	// 	" #    ##    ##    ###",
	// 	"# #  #  #  #  #  # # ",
	// 	"##    ##    ##    ###",
	// 	"# #  #  #  #  #  #   ",
	// }

	// Let's try a single scan
	grid2 := Tile{raw: searchGrid}
	// fmt.Println("testmonster:")
	// scanSea(testmonster, monster)

	i := 0
	for {
		i++
		res, scannedArea := scanSea(grid2.raw, monster)
		signal := strings.Join(scannedArea, "")
		answer := strings.Count(signal, "#")
		if res > 0 {
			fmt.Println("Problem 2: found ", res, "monsters on", strings.Count(signal, "s"), "tiles, leaving", answer, "rough waves in total scanned fields of", len(signal))
			// for _, line := range scannedArea {
			// 	fmt.Println(line)
			// }
			break
		}
		grid2.rotate(1)
		if i%5 == 0 {
			grid2.flip()
		}
		if i == 20 {
			// fmt.Print("\n")
			// for i, line := range grid2.raw {
			// 	fmt.Printf("%3d - %s\n", i, line)
			// }
			// grid2.rotate(1)
			// for i, line := range grid2.raw {
			// 	fmt.Printf("%3d - %s\n", i, line)
			// }
			fmt.Println()
			break
		}
	}

	// Count remaining # tiles
	// 2231 = too high
	// 1912 = too high
	// 1897 = too high
	// 1972 = wrong
	// 1792 = right
	fmt.Println("Max answer:", strings.Count(strings.Join(grid2.raw, ""), "#"))
}

func scanSea(area []string, monster []string) (int, []string) {
	//-> create a new grid with (., #, S) tiles
	scannedGrid := make([]string, len(area))
	copy(scannedGrid, area)

	monsterWidth := len(monster[0])
	monsterHeight := len(monster)

	monsterCount := 0
	for y := 0; y < len(area)-monsterHeight+1; y++ {
		for x := 0; x < len(area[0])-monsterWidth+1; x++ {
			minigrid := []string{}
			for i := 0; i < monsterHeight; i++ {
				minigrid = append(minigrid, area[y+i][x:x+monsterWidth])
			}
			if scan(minigrid, monster) {
				monsterCount++
				for py, line := range monster {
					for px, p := range line {
						if string(p) == " " {
							continue
						}
						scannedGrid[y+py] = fmt.Sprintf("%s%s%s", scannedGrid[y+py][0:x+px], "s", scannedGrid[y+py][x+px+1:len(scannedGrid[y])])
					}
				}
			}
		}
	}
	fmt.Print(".")
	return monsterCount, scannedGrid
}

func scan(area []string, pattern []string) bool {
	for y, line := range pattern {
		for x, p := range line {
			if string(p) == " " {
				continue // skip if mask is none
			}
			if string(area[y][x]) == "#" {
				continue // only continue if mask matches
			}
			return false
		}
	}
	return true
}

func (t *Tile) print() {
	for _, l := range t.raw {
		fmt.Println(l)
	}
	fmt.Println()
}

func (t *Tile) initEdges() {
	t.edges = []string{}
	l, r := "", ""
	for _, line := range t.raw {
		l = fmt.Sprintf("%s%s", string(line[0]), l)
		r += string(line[len(line)-1])
	}
	t.edges = append(t.edges, t.raw[0], r, utils.Reverse(t.raw[len(t.raw)-1]), l)
}

// flip flips left with right
func (t *Tile) flip() {
	newRaw := []string{}
	for _, l := range t.raw {
		newRaw = append(newRaw, utils.Reverse(l))
	}
	t.raw = newRaw
	t.initEdges()
}

func (t *Tile) rotate(n int) {
	n = n % 4 // n is number of clockwise rotations by 90% (can be negative for CCW)
	if n < 0 {
		n = n + 4
	}

	for x := 0; x < n; x++ {
		newRaw := []string{}
		for col := 0; col < len(t.raw[0]); col++ {
			newRow := ""
			for row := 0; row < len(t.raw); row++ {
				newRow = fmt.Sprintf("%s%s", string(t.raw[row][col]), newRow)
			}
			newRaw = append(newRaw, newRow)
		}
		t.raw = newRaw
	}
	t.initEdges()
}

// Match returns -1, -1 if there is no match
// If there is a match, the first return value indicates the edge on the first tile:
// 0 - top edge, 1 - right edge, 2- bottom edge, 3- left edge
// reverse edges (from the second tile) are in the same order, starting at 4
func (t *Tile) match(t2 *Tile) (int, int) {
	// Try to match normal edges
	for i, e1 := range t.edges {
		for j, e2 := range t2.edges {
			if e1 == e2 {
				return i, j + 4
			}
			if e1 == utils.Reverse(e2) {
				return i, j
			}
		}
	}
	return -1, -1
}

func allocateTiles(ct *Tile, grid map[point]*Tile) {
	// - for each neighbor, find how it fits, rotate/flip accordingly, allocate on grid
	for _, n := range ct.neighbours {
		// - skip of already allocated (tile.pos != nil)
		if n.allocated {
			continue // skip already allocated tiles
		}

		// Flip if needed
		mySide, matchSide := ct.match(n)
		if matchSide > 3 {
			n.flip()
			mySide, matchSide = ct.match(n)
		}
		// Rotate the tile (always +/- 2) (so, plus 2 > mod 4)
		n.rotate(mySide - matchSide + 2)

		// allocate to the grid
		switch mySide {
		case 0:
			// Above me
			n.pos = point{ct.pos.x, ct.pos.y + 1}
		case 1:
			// To my right
			n.pos = point{ct.pos.x + 1, ct.pos.y}

		case 2:
			// Below me
			n.pos = point{ct.pos.x, ct.pos.y - 1}

		case 3:
			// To my left
			n.pos = point{ct.pos.x - 1, ct.pos.y}
		}
		grid[n.pos] = n
		n.allocated = true

		allocateTiles(n, grid)
	}
}

func scatterPlot(data map[point]*Tile, filename string) {

	// tilePoints returns some  x, y points for the tiles
	tilePoints := func(d map[point]*Tile) plotter.XYs {
		pts := make(plotter.XYs, len(d))
		i := 0
		for _, v := range d {
			pts[i].X = float64(v.pos.x)
			pts[i].Y = float64(v.pos.y)
			i++
		}
		return pts
	}

	scatterData := tilePoints(data)

	p, err := plot.New()
	if err != nil {
		log.Panic(err)
	}
	p.Title.Text = "Allocated IDs"
	p.X.Label.Text = "X"
	p.Y.Label.Text = "Y"
	p.Add(plotter.NewGrid())

	s, err := plotter.NewScatter(scatterData)
	if err != nil {
		log.Panic(err)
	}
	s.GlyphStyle.Color = color.RGBA{R: 255, B: 128, A: 255}
	s.GlyphStyle.Radius = vg.Points(2)

	p.Add(s)

	err = p.Save(400, 400, filename)
	if err != nil {
		log.Panic(err)
	}
}
