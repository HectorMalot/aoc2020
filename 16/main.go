package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/hectormalot/aoc2020/utils"
)

type rule struct {
	name    string
	ranges  []validrange
	columns map[int]bool
}

type validrange struct {
	min, max int
}

type ticket struct {
	values []int
}

/*
To those that enter here: This is some of the worst code I've managed
to stick together in quite some time. I'm sorry!
*/

func main() {
	// Parse input
	lines, _ := utils.ImportToStringSlice("input.txt")

	myticket := ticket{}
	tickets := []ticket{}
	rules := []rule{}

	parsing := "rules"

	for _, line := range lines {
		switch parsing {
		case "rules":
			if line == "" {
				continue
			}
			if line == "your ticket:" {
				parsing = "myticket"
				continue
			}
			rules = append(rules, newRule(line))
		case "myticket":
			if line == "" {
				continue
			}
			if line == "nearby tickets:" {
				parsing = "tickets"
				continue
			}
			myticket = newTicket(line)
		case "tickets":
			tickets = append(tickets, newTicket(line))
		}
	}

	// validate tickets (problem 1)
	sum := 0
	validTickets := []ticket{}
	for _, t := range tickets {
		res := t.validate(rules)
		if res > 0 {
			sum += res
			continue
		}
		validTickets = append(validTickets, t)
	}
	fmt.Println("Problem 1:", sum)

	// determine possible valid columns by rule
	for _, rule := range rules {
		// fmt.Println("Checking rule:", rule.name)
		// scan by column across all valid tickets
		for i := 0; i < len(myticket.values)-1; i++ {
			valid := true
			for _, t := range validTickets {
				if !rule.validate(t.values[i]) {
					valid = false
				}
			}
			// fmt.Println("Column", i, ":", valid)
			rule.columns[i] = valid
		}
	}

	// Exclude columns 1 by 1
	sort.Slice(rules, func(i, j int) bool { return rules[i].numValid() < rules[j].numValid() })

	// Allocate column names to specific column IDs
	result := make(map[int]string)
	for _, rule := range rules {
		for k, v := range rule.columns {
			if v {
				result[k] = rule.name
				// remove the option from all other rules
				for _, rule := range rules {
					rule.columns[k] = false
				}
			}
		}
	}

	// Multiply the numbers that start with departure
	mul := 1
	for k, v := range result {
		v2 := strings.Split(v, " ")
		if v2[0] == "departure" {
			mul *= myticket.values[k]
		}
	}
	fmt.Println("Problem 2:", mul)

}

func newRule(line string) rule {
	split1 := strings.Split(line, ":")
	name := split1[0]

	split2 := strings.Split(strings.ReplaceAll(split1[1], " ", ""), "or")
	validRanges := []validrange{}
	for _, str := range split2 {
		validRanges = append(validRanges, newValidRange(str))
	}
	return rule{name: name, ranges: validRanges, columns: make(map[int]bool)}
}

func newTicket(line string) ticket {
	vals := strings.Split(line, ",")
	res := []int{}
	for _, v := range vals {
		i, _ := strconv.Atoi(v)
		res = append(res, i)
	}
	return ticket{res}
}

func newValidRange(str string) validrange {
	vr := validrange{}
	fmt.Sscanf(str, "%d-%d", &vr.min, &vr.max)
	return vr
}

func (t ticket) validate(sr []rule) int {
	sum := 0
	for _, v := range t.values {
		valid := false
		for _, rule := range sr {
			if rule.validate(v) {
				valid = true
			}
		}
		if !valid {
			sum += v
		}
	}
	return sum
}

func (r rule) validate(v int) bool {
	for _, r := range r.ranges {
		if v >= r.min && v <= r.max {
			return true
		}
	}
	return false
}

func (r *rule) numValid() int {
	numValid := 0
	for _, v := range r.columns {
		if v {
			numValid++
		}
	}
	return numValid
}

/*
--- Day 16: Ticket Translation ---

As you're walking to yet another connecting flight, you realize that one of the legs of your re-routed trip coming up is on a high-speed train. However, the train ticket you were given is in a language you don't understand. You should probably figure out what it says before you get to the train station after the next flight.

Unfortunately, you can't actually read the words on the ticket. You can, however, read the numbers, and so you figure out the fields these tickets must have and the valid ranges for values in those fields.

You collect the rules for ticket fields, the numbers on your ticket, and the numbers on other nearby tickets for the same train service (via the airport security cameras) together into a single document you can reference (your puzzle input).

The rules for ticket fields specify a list of fields that exist somewhere on the ticket and the valid ranges of values for each field. For example, a rule like class: 1-3 or 5-7 means that one of the fields in every ticket is named class and can be any value in the ranges 1-3 or 5-7 (inclusive, such that 3 and 5 are both valid in this field, but 4 is not).

Each ticket is represented by a single line of comma-separated values. The values are the numbers on the ticket in the order they appear; every ticket has the same format. For example, consider this ticket:

.--------------------------------------------------------.
| ????: 101    ?????: 102   ??????????: 103     ???: 104 |
|                                                        |
| ??: 301  ??: 302             ???????: 303      ??????? |
| ??: 401  ??: 402           ???? ????: 403    ????????? |
'--------------------------------------------------------'
Here, ? represents text in a language you don't understand. This ticket might be represented as 101,102,103,104,301,302,303,401,402,403; of course, the actual train tickets you're looking at are much more complicated. In any case, you've extracted just the numbers in such a way that the first number is always the same specific field, the second number is always a different specific field, and so on - you just don't know what each position actually means!

Start by determining which tickets are completely invalid; these are tickets that contain values which aren't valid for any field. Ignore your ticket for now.

For example, suppose you have the following notes:

class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12
It doesn't matter which position corresponds to which field; you can identify invalid nearby tickets by considering only whether tickets contain values that are not valid for any field. In this example, the values on the first nearby ticket are all valid for at least one field. This is not true of the other three nearby tickets: the values 4, 55, and 12 are are not valid for any field. Adding together all of the invalid values produces your ticket scanning error rate: 4 + 55 + 12 = 71.

Consider the validity of the nearby tickets you scanned. What is your ticket scanning error rate?
*/
