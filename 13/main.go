package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/hectormalot/aoc2020/utils"
)

func main() {
	lines, _ := utils.ImportToStringSlice("input.txt")
	now, _ := strconv.Atoi(lines[0])
	busses := strings.Split(lines[1], ",")

	// Problem 1
	for _, bus := range busses {
		if bus == "x" {
			continue
		}
		n, _ := strconv.Atoi(bus)
		// fmt.Println(bus, "is prime:", big.NewInt(int64(n)).ProbablyPrime(2))
		rides := now / n
		fmt.Println("Time to next for bus:", bus, "is:", n*(rides+1)-now)
	}

	// Problem 2
	// Insights:
	// 1. All line numbers are prime
	// 2. A match of 2 line numbers (a, b) will therefore repeat every a*b
	// Approach:
	// 3. Step forward by (a) until we match the next number (b)
	// 4. Step forward by (a*b) until we match the next number (c), etc.
	fmt.Println("\n\n-----Solving for problem 2-----")
	sol := 0
	step := 0
	for i, bus := range busses {
		if bus == "x" {
			continue
		}
		n, _ := strconv.Atoi(bus)
		if i == 0 { // setup the first bus
			step = n
			continue
		}
		fmt.Println("calling with:", sol, step, i, n)
		sol = findNextMatch(sol, step, i, n)
		step = step * n
	}

}

func findNextMatch(start, step, diff, target int) int {
	n := start
	for {
		if n%target == target-(diff%target) {
			return n // Found solution for next bus
		}
		n += step
	}
}
