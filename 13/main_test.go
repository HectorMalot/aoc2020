package main

import "testing"

func TestFinder(t *testing.T) {
	// 643 op 17
	res := findNextMatch(0, 17, 17, 643) // 10914
	if res != 10914 {
		t.Error(res)
	}
	// 41 op 7
	res = findNextMatch(10914, 17*643, 7, 41) // 273258
	if res != 273258 {
		t.Error(res)
	}
	// 23 op 25
	res = findNextMatch(273258, 17*643*41, 25, 23) // 7443994
	if res != 7443994 {
		t.Error(res)
	}

}
