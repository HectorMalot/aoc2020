package main

import (
	"fmt"

	set "github.com/deckarep/golang-set"

	"gitlab.com/hectormalot/aoc2020/utils"
)

type game struct {
	deck1, deck2 []int
	history      set.Set
	depth        int
}

func main() {
	// Parse input
	deck1, deck2 := parse("input.txt")
	g := game{deck1, deck2, set.NewSet(), 0}
	g.play()
	winner := g.playRecursive()
	fmt.Println("Recursive winner:", winner, "with score:", g.score(winner))
}

func parse(path string) ([]int, []int) {
	lines, _ := utils.ImportToStringSlice(path)
	deck1, deck2 := []int{}, []int{}
	for _, line := range lines {
		if string(line) == "Player 1:" {
			continue
		}
		if string(line) == "Player 2:" {
			deck1 = deck2
			deck2 = []int{}
			continue
		}
		if string(line) == "" {
			continue
		}
		deck2 = append(deck2, utils.MustAtoi(line))
	}
	return deck1, deck2
}

func (g *game) play() {
	c1, c2 := make(chan int, len(g.deck1)+len(g.deck2)), make(chan int, len(g.deck1)+len(g.deck2))

	// Start the game for player 1
	for _, v := range g.deck1 {
		c1 <- v
	}

	// Start the game for player 2
	for _, v := range g.deck2 {
		c2 <- v
	}

	// Game loop for problem 1
	for {
		if len(c1) == 0 || len(c2) == 0 {
			fmt.Println("Game completed")
			break
		}
		card1 := <-c1
		card2 := <-c2

		if card1 > card2 {
			c1 <- card1
			c1 <- card2
			continue
		}
		c2 <- card2
		c2 <- card1
	}

	// print the scores
	var c chan int
	if len(c1) > 0 {
		c = c1
	} else {
		c = c2
	}
	close(c1)
	close(c2)
	i, sum := len(c), 0
	for v := range c {
		sum += v * i
		i--
	}
	fmt.Println("Problem 1:", sum)
}

func (g *game) playRecursive() int {
	// round := 0
	for {
		// round++
		// fmt.Println("\n-- Round", round, " (Game", g.depth+1, ") --")
		// fmt.Println("Player 1's deck:", g.deck1)
		// fmt.Println("Player 2's deck:", g.deck2)

		if len(g.deck2) == 0 {
			return 1 // Player one wins game
		}
		if len(g.deck1) == 0 {
			return 2 // Player two wins game
		}

		// (1) Check for infinite loop
		fingerprint := ""
		fingerprintSlice := append(g.deck1, 999) // player separator
		fingerprintSlice = append(fingerprintSlice, g.deck2...)
		for _, v := range fingerprintSlice {
			fingerprint = fmt.Sprintf("%s,%d", fingerprint, v)
		}
		if g.history.Contains(fingerprint) {
			// player 1 wins the game
			return 1
		}
		g.history.Add(fingerprint)

		// (2) Both players draw a card
		card1, card2 := g.deck1[0], g.deck2[0]

		// (3) IF both card values are < len(deck), play a recursive game to determine the winner
		if card1 < len(g.deck1) && card2 < len(g.deck2) {
			subdeck1, subdeck2 := make([]int, card1), make([]int, card2)
			copy(subdeck1, g.deck1[1:card1+1])
			copy(subdeck2, g.deck2[1:card2+1])
			subgame := game{subdeck1, subdeck2, set.NewSet(), g.depth + 1}
			g.next(subgame.playRecursive())
			continue
		}

		// (4) Otherwise, play a normal game
		if card1 > card2 {
			g.next(1) // player 1 wins card
			continue
		}
		g.next(2) // player 2 wins card

		/*
			* Before either player deals a card, if there was a previous round in this game that had exactly the same cards in the same order in the same players' decks, the game instantly ends in a win for player 1. Previous rounds from other games are not considered. (This prevents infinite games of Recursive Combat, which everyone agrees is a bad idea.)
			* Otherwise, this round's cards must be in a new configuration; the players begin the round by each drawing the top card of their deck as normal.
			* If both players have at least as many cards remaining in their deck as the value of the card they just drew, the winner of the round is determined by playing a new game of Recursive Combat (see below).
			Otherwise, at least one player must not have enough cards left in their deck to recurse; the winner of the round is the player with the higher-value card.
		*/
	}
}

func (g *game) next(winner int) {
	if winner == 1 {
		g.deck1 = append(g.deck1, g.deck1[0], g.deck2[0])
	} else {
		g.deck2 = append(g.deck2, g.deck2[0], g.deck1[0])
	}
	g.deck1 = g.deck1[1:]
	g.deck2 = g.deck2[1:]
}

func (g *game) score(winner int) int {
	winningDeck := []int{}
	if winner == 1 {
		winningDeck = g.deck1
	} else {
		winningDeck = g.deck2
	}

	i, sum := len(winningDeck), 0
	for _, v := range winningDeck {
		sum += v * i
		i--
	}
	return sum
}
