package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/hectormalot/aoc2020/utils"
)

func main() {
	lines, _ := utils.ImportToStringSlice("input.txt")

	c1 := computer{memory: make(map[uint64]uint64)}
	c2 := computer{memory: make(map[uint64]uint64)}
	for _, line := range lines {
		c1.exec(line)
		c2.execV2(line)
	}

	fmt.Println("Problem 1:", c1.sum())
	fmt.Println("Problem 2:", c2.sum())
}

type computer struct {
	m0     uint64
	m1     uint64
	mRaw   string
	memory map[uint64]uint64
}

func (c *computer) exec(input string) {
	if input[0:2] == "ma" {
		// mask op
		mask := input[7:]
		c.m0, _ = strconv.ParseUint(strings.ReplaceAll(mask, "X", "1"), 2, 64)
		c.m1, _ = strconv.ParseUint(strings.ReplaceAll(mask, "X", "0"), 2, 64)
	} else {
		// memory op
		var address, value uint64
		fmt.Sscanf(input, "mem[%d] = %d", &address, &value)
		c.memory[address] = (value & c.m0) | c.m1
	}
}

func (c *computer) execV2(input string) {
	if input[0:2] == "ma" {
		// mask op
		c.mRaw = input[7:]
		c.m0, _ = strconv.ParseUint(strings.ReplaceAll(c.mRaw, "X", "1"), 2, 64)
		c.m1, _ = strconv.ParseUint(strings.ReplaceAll(c.mRaw, "X", "0"), 2, 64)
	} else {
		// memory op
		var address, value uint64
		fmt.Sscanf(input, "mem[%d] = %d", &address, &value)
		for _, a := range c.findAddresses(address) {
			c.memory[a] = value
		}
	}

}

func (c *computer) findAddresses(input uint64) []uint64 {
	// convert input to string
	s := []byte(fmt.Sprintf("%036b", input))

	// create the master set
	for i := range s {
		if string(c.mRaw[i]) == "1" || string(c.mRaw[i]) == "X" {
			s[i] = c.mRaw[i]
		}
	}

	// generate the set of possible addresses
	stringResult := c.generateAddresses(string(s))

	// convert to uint64
	intResult := []uint64{}
	for _, r := range stringResult {
		i, _ := strconv.ParseUint(r, 2, 64)
		intResult = append(intResult, i)
	}

	return intResult
}

func (c *computer) generateAddresses(input string) []string {
	// replace the first X found with 1 and 0 and return
	res := []string{}
	if !strings.Contains(input, "X") {
		return []string{input} // we're done here
	}
	res = append(res, c.generateAddresses(strings.Replace(input, "X", "1", 1))...)
	res = append(res, c.generateAddresses(strings.Replace(input, "X", "0", 1))...)
	return res
}

func (c *computer) sum() uint64 {
	sum := uint64(0)
	for _, v := range c.memory {
		sum += v
	}
	return sum
}

/*
000000000000000000000000000000011010  (decimal 26)
000000000000000000000000000000011011  (decimal 27)
000000000000000000000000000000111010  (decimal 58)
000000000000000000000000000000111011  (decimal 59)
*/
