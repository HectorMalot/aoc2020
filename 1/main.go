package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2020/utils"
)

func main() {
	// problem1()
	problem2()
}

/*
Before you leave, the Elves in accounting just need you to fix your expense report (your puzzle input); apparently, something isn't quite adding up.

Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.

For example, suppose your expense report contained the following:

1721
979
366
299
675
1456
In this list, the two entries that sum to 2020 are 1721 and 299. Multiplying them together produces 1721 * 299 = 514579, so the correct answer is 514579.

Of course, your expense report is much larger. Find the two entries that sum to 2020; what do you get if you multiply them together?

answer: 793524
*/
func problem1() {
	entries, _ := utils.ImportToIntSlice("./input1.txt")

	// Solution:
	// 1. Loop over all entries
	// 2. for each entry, loop over all remaining entries
	// 3. Find the 2 entries that sum to 2020
	// 4. Print the multiplication of the 2 entries
	for i, e1 := range entries {
		for j := i + 1; j < len(entries); j++ {
			if e1+entries[j] == 2020 {
				fmt.Println(e1 * entries[j])
			}
		}
	}
}

/*
The Elves in accounting are thankful for your help; one of them even offers you a starfish coin they had left over from a past vacation. They offer you a second one if you can find three numbers in your expense report that meet the same criteria.

Using the above example again, the three entries that sum to 2020 are 979, 366, and 675. Multiplying them together produces the answer, 241861950.

In your expense report, what is the product of the three entries that sum to 2020?

Answer: 61515678
*/
func problem2() {
	entries, _ := utils.ImportToIntSlice("./input1.txt")

	// Solution:
	// 1. Loop over all entries
	// 2. for each entry, loop over all remaining entries
	// 3. Find the 3 entries that sum to 2020
	// 4. Print the multiplication of the 3 entries
	for i, e1 := range entries {
		for j := i + 1; j < len(entries); j++ {
			for h := j + 2; h < len(entries); h++ {
				if e1+entries[j]+entries[h] == 2020 {
					fmt.Println(e1 * entries[j] * entries[h])
				}
			}
		}
	}
}
