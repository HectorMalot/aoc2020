package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

func main() {
	// use input1.txt for problem 1
	// Solution for problem 2 is a bit of a hack
	input, _ := ioutil.ReadFile("input.txt")
	rules, observations := parse(input)

	// Build the regex
	filter := rules["0"]
	re := regexp.MustCompile(`\d+`)
	for re.MatchString(filter) {
		filter = re.ReplaceAllStringFunc(filter, func(s string) string {
			return fmt.Sprintf("(%s)", rules[s])
		})
	}
	filter = strings.ReplaceAll(filter, "(a)", "a")
	filter = strings.ReplaceAll(filter, "(b)", "b")
	filter = strings.ReplaceAll(filter, " ", "")
	filter = fmt.Sprintf("^%s$", filter)

	// Validate each line
	count := 0
	re = regexp.MustCompile(filter)
	for _, obs := range observations {
		if re.MatchString(obs) {
			count++
		}
	}
	fmt.Println("Solution Problem 1:", count) // 173

	// Problem 2:
	rules["8"] = "42+"
	rules["11"] = "42 31 | 42 42 31 31 | 42 42 42 31 31 31 | 42 42 42 42 31 31 31 31"

	// Build the regex
	filter = rules["0"]
	re = regexp.MustCompile(`\d+`)
	for re.MatchString(filter) {
		filter = re.ReplaceAllStringFunc(filter, func(s string) string {
			return fmt.Sprintf("(%s)", rules[s])
		})
	}
	filter = strings.ReplaceAll(filter, "(a)", "a")
	filter = strings.ReplaceAll(filter, "(b)", "b")
	filter = strings.ReplaceAll(filter, " ", "")
	filter = fmt.Sprintf("^%s$", filter)

	// Validate each line
	count = 0
	re = regexp.MustCompile(filter)
	for _, obs := range observations {
		if re.MatchString(obs) {
			count++
		}
	}
	fmt.Println("Solution Problem 2:", count) // 367

}

func parse(b []byte) (map[string]string, []string) {
	split := strings.Split(string(b), "\n\n")
	rules, observations := strings.Split(split[0], "\n"), strings.Fields(split[1])

	res := make(map[string]string)
	for _, rule := range rules {
		split = strings.Split(rule, ":")
		key := split[0]
		res[key] = strings.ReplaceAll(strings.TrimSpace(split[1]), "\"", "")
	}

	return res, observations
}
