package main

import "fmt"

func main() {
	pk1, pk2 := 6929599, 2448427 // 2448427 loops: 8652834
	// pk1, pk2 := 5764801, 17807724 // test inputs (8 and 11 loops) , final key: 14897079

	// Find a solution for 1 of the 2 keys
	loops, key := search(7, 20201227, []int{pk1, pk2})
	fmt.Println("Found solution for key:", key, ", loops:", loops)

	// Calculate the shared key
	if key == pk1 {
		fmt.Println("Shared key:", transform(pk2, 20201227, loops))
		return
	}
	fmt.Println("Shared key:", transform(pk1, 20201227, loops))
}

func search(base, mod int, answers []int) (int, int) {
	i := 1
	exp := base
	for {
		exp = exp % mod
		for _, a := range answers {
			if exp == a {
				return i, a
			}
		}
		exp = exp * base
		i++
		if i == 1000 {
			fmt.Println(i, exp)
		}
	}
}

func transform(input, mod, loops int) int {
	exp := 1
	for i := 0; i < loops; i++ {
		exp = exp * input % mod
	}
	return exp
}
