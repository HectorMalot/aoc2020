package main

import (
	"fmt"
	"strconv"
	"strings"

	set "github.com/deckarep/golang-set"
	"gitlab.com/hectormalot/aoc2020/utils"
)

var (
	errInfiniteLoop = fmt.Errorf("Infinite Loop")
)

func main() {
	// Import the 'program'
	lines, _ := utils.ImportToStringSlice("input.txt")
	p := newProgram()
	for _, line := range lines {
		parts := strings.Split(line, " ")
		arg, _ := strconv.Atoi(parts[1])
		p.addInstruction(instruction{parts[0], arg})
	}

	// Problem 1
	err := p.exec()
	fmt.Println(err)
	fmt.Println("Value for problem 1:", p.acc)

	// Problem 2
	// One could work backwards from the failing instruction, but why not brute force?
	// Let's generate all possible solutions
	p.reset()
	programs := []*program{}
	for i := 0; i < len(p.buffer)-1; i++ {
		programs = append(programs, p.copyWithSwitch(i))
	}

	// Let's run them one by one until we find one that succeeds
	for _, p := range programs {
		err := p.exec()
		if err == errInfiniteLoop {
			// fmt.Println("program", i, "did not work. Acc value:", p.acc)
			continue // This program did not work
		}
		fmt.Println("Value for problem 2:", p.acc)
		break
	}
}

type instruction struct {
	ins string
	arg int
}

type program struct {
	buffer []instruction
	pos    int
	acc    int
}

func newProgram() *program {
	return &program{buffer: []instruction{}, pos: 0, acc: 0}
}

func (p *program) addInstruction(i instruction) {
	p.buffer = append(p.buffer, i)
}

func (p *program) reset() {
	p.acc = 0
	p.pos = 0
}

func (p *program) step() bool {
	if p.pos >= len(p.buffer) {
		return false
	}
	i := p.buffer[p.pos]
	switch i.ins {
	case "nop":
		p.pos++
	case "acc":
		p.acc += i.arg
		p.pos++
	case "jmp":
		p.pos += i.arg
	}
	return true
}

func (p *program) exec() error {
	// Run the program until we step back onto an earlier executed statement
	locs := set.NewSet(0)
	for p.step() {
		// Stop execution if we step back to an already executed instruction
		if locs.Contains(p.pos) {
			return errInfiniteLoop
		}
		locs.Add(p.pos)
	}
	return nil
}

func (p *program) copyWithSwitch(i int) *program {
	newProg := *p
	newProg.buffer = make([]instruction, len(p.buffer))
	copy(newProg.buffer, p.buffer)

	if p.buffer[i].ins == "jmp" {
		newProg.buffer[i].ins = "nop"
	}
	if p.buffer[i].ins == "nop" {
		newProg.buffer[i].ins = "jmp"
	}

	return &newProg
}

/*
Your flight to the major airline hub reaches cruising altitude without incident. While you consider checking the in-flight menu for one of those drinks that come with a little umbrella, you are interrupted by the kid sitting next to you.

Their handheld game console won't turn on! They ask if you can take a look.

You narrow the problem down to a strange infinite loop in the boot code (your puzzle input) of the device. You should be able to fix it, but first you need to be able to run the code in isolation.

The boot code is represented as a text file with one instruction per line of text. Each instruction consists of an operation (acc, jmp, or nop) and an argument (a signed number like +4 or -20).

acc increases or decreases a single global value called the accumulator by the value given in the argument. For example, acc +7 would increase the accumulator by 7. The accumulator starts at 0. After an acc instruction, the instruction immediately below it is executed next.
jmp jumps to a new instruction relative to itself. The next instruction to execute is found using the argument as an offset from the jmp instruction; for example, jmp +2 would skip the next instruction, jmp +1 would continue to the instruction immediately below it, and jmp -20 would cause the instruction 20 lines above to be executed next.
nop stands for No OPeration - it does nothing. The instruction immediately below it is executed next.
For example, consider the following program:

nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6
These instructions are visited in this order:

nop +0  | 1
acc +1  | 2, 8(!)
jmp +4  | 3
acc +3  | 6
jmp -3  | 7
acc -99 |
acc +1  | 4
jmp -4  | 5
acc +6  |
First, the nop +0 does nothing. Then, the accumulator is increased from 0 to 1 (acc +1) and jmp +4 sets the next instruction to the other acc +1 near the bottom. After it increases the accumulator from 1 to 2, jmp -4 executes, setting the next instruction to the only acc +3. It sets the accumulator to 5, and jmp -3 causes the program to continue back at the first acc +1.

This is an infinite loop: with this sequence of jumps, the program will run forever. The moment the program tries to run any instruction a second time, you know it will never terminate.

Immediately before the program would run an instruction a second time, the value in the accumulator is 5.

Run your copy of the boot code. Immediately before any instruction is executed a second time, what value is in the accumulator?
*/

/*
--- Part Two ---

After some careful analysis, you believe that exactly one instruction is corrupted.

Somewhere in the program, either a jmp is supposed to be a nop, or a nop is supposed to be a jmp. (No acc instructions were harmed in the corruption of this boot code.)

The program is supposed to terminate by attempting to execute an instruction immediately after the last instruction in the file. By changing exactly one jmp or nop, you can repair the boot code and make it terminate correctly.

For example, consider the same program from above:

nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6
If you change the first instruction from nop +0 to jmp +0, it would create a single-instruction infinite loop, never leaving that instruction. If you change almost any of the jmp instructions, the program will still eventually find another jmp instruction and loop forever.

However, if you change the second-to-last instruction (from jmp -4 to nop -4), the program terminates! The instructions are visited in this order:

nop +0  | 1
acc +1  | 2
jmp +4  | 3
acc +3  |
jmp -3  |
acc -99 |
acc +1  | 4
nop -4  | 5
acc +6  | 6
After the last instruction (acc +6), the program terminates by attempting to run the instruction below the last instruction in the file. With this change, after the program terminates, the accumulator contains the value 8 (acc +1, acc +1, acc +6).

Fix the program so that it terminates normally by changing exactly one jmp (to nop) or nop (to jmp). What is the value of the accumulator after the program terminates?
*/
