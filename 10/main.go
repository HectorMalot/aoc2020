package main

import (
	"fmt"
	"sort"
	"time"

	"gitlab.com/hectormalot/aoc2020/utils"
)

func main() {
	t1 := time.Now()
	lines, _ := utils.ImportToIntSlice("input.txt")
	adapters := sort.IntSlice(lines)
	// adapters = append(adapters, utils.Max(adapters)+3)
	adapters.Sort()

	// Problem 1
	// fmt.Println(adapters)
	counts := make(map[int]int)
	prev := 0
	for _, a := range adapters {
		counts[a-prev]++
		prev = a
	}
	counts[3]++
	fmt.Println("Problem 1: ", counts[1]*counts[3])

	// Problem 2
	// Create the full view
	adapters = append(adapters, utils.Max(adapters)+3)
	adapters = append([]int{0}, adapters...)

	// Split by gaps of 3 ==> [][]int
	// calculation of options for each []int
	subgroups := splitList(adapters)

	// multiply it out!
	answer := 1
	for _, g := range subgroups {
		answer *= combinations(g)
	}

	fmt.Println("Problem 2:", answer)
	t2 := time.Now()
	fmt.Println("Time:", t2.Sub(t1))
}

func splitList(l []int) [][]int {
	result := [][]int{}
	sublist := sort.IntSlice{}
	prev := 0
	for _, v := range l {
		if v == 0 {
			sublist = append(sublist, v)
			prev = v
			continue
		}
		if (v - prev) == 3 {
			// start a new slice
			result = append(result, sublist)
			sublist = sort.IntSlice{}
		}
		// add to current slice
		sublist = append(sublist, v)
		prev = v
	}
	return result
}

func combinations(l []int) int {
	if len(l) == 1 {
		return 1
	}
	if len(l) == 2 {
		return 1
	}
	if len(l) == 3 {
		// check if middle one can be missed
		if utils.Max(l)-utils.Min(l) <= 3 {
			return 2
		}
		return 1
	}
	// Generic solutions are nice, hard-coded is simple, and I need to beat dimi :)
	if len(l) == 4 {
		// 4 is interesting. I can either remove 1 or 2 in the middle, so either 2 options, or 3 options
		if utils.Max(l)-utils.Min(l) == 3 {
			return 4 // remove left, right or both
		}
		return 3 // remove left or right
	}
	if len(l) == 5 {
		// if it is a continuous line, I can remove at least 1 (3 options)
		// or remove any 2 in the middle (another 3 options)
		if utils.Max(l)-utils.Min(l) == 4 {
			return 7
		}
		return 99 // Turns out my input doesn't have groups of five that are not continuous
	}
	if len(l) > 5 {
		panic("oh noes")
	}
	return 99 // This never happens (these are the best comments in production...)
}
