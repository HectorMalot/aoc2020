package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2020/utils"
)

func main() {
	lines, _ := utils.ImportToStringSlice("input.txt")

	// Problem 1: step until done
	prev := -1
	for {
		lines = step(lines)
		c := count(lines)
		if prev == c {
			break
		}
		prev = c
	}
	fmt.Println("Problem 1:", prev)

	// Problem 2: step until done (for loops galore!)
	lines, _ = utils.ImportToStringSlice("input.txt")
	prev = -1
	for {
		lines = step2(lines)
		c := count(lines)
		if prev == c {
			break
		}
		prev = c
	}
	fmt.Println("Problem 2:", prev)

}

/*
Rules:
If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
Otherwise, the seat's state does not change.
*/

func step(lines []string) []string {
	next := []string{}
	for i, line := range lines {
		// fmt.Println("stepping line", i)
		newline := ""
		for j, c := range line {
			var s string
			// floors do nothing
			if string(c) == "." {
				s = "."
			}
			// empty seats become occupied if none around are occupied
			// occupied seats become empty if >3 around are occupied
			if string(c) != "." {
				occupied := 0
				for k := i - 1; k <= i+1; k++ {
					if k < 0 || k >= len(lines) {
						continue
					}
					for l := j - 1; l <= j+1; l++ {
						if l < 0 || l >= len(line) {
							continue
						}
						if k == i && l == j {
							continue
						}
						// fmt.Println("idexes (line, char):", k, l)
						if string(lines[k][l]) == "#" {
							occupied++
						}
					}
				}
				if (occupied > 3) || (occupied > 0 && string(c) == "L") {
					s = "L"
				} else {
					s = "#"
				}
			}
			// Add the result!
			newline = fmt.Sprintf("%s%s", newline, s)
		}
		next = append(next, newline)
	}
	return next
}

func step2(lines []string) []string {
	next := []string{}
	for i, line := range lines {
		newline := ""
		for j, c := range line {
			var s string
			// floors do nothing
			if string(c) == "." {
				s = "."
			}
			if string(c) == "L" {
				if countAdjecentSeats(lines, j, i) == 0 {
					s = "#" // occupy the seat
				} else {
					s = "L" // remain empty if any seen seat is occupied
				}
			}
			if string(c) == "#" {
				if countAdjecentSeats(lines, j, i) < 5 {
					s = "#" // Remain occupied
				} else {
					s = "L" // become empty if more than 4 adjencent seats are occupied
				}
			}

			// Add the result!
			newline = fmt.Sprintf("%s%s", newline, s)
		}
		next = append(next, newline)
	}
	return next
}

func count(lines []string) (occupied int) {
	for _, line := range lines {
		for _, c := range line {
			if string(c) == "#" {
				occupied++
			}
		}
	}
	return occupied
}

func countAdjecentSeats(seats []string, xpos, ypos int) int {
	occupied := 0

	// Search from east counter clockwise
	sdx := []int{1, 1, 0, -1, -1, -1, 0, 1}
	sdy := []int{0, 1, 1, 1, 0, -1, -1, -1}
	for i, dx := range sdx {
		dy := sdy[i]

		x, y := xpos, ypos
		// walk in this direction until we find a chair
		for {
			x += dx
			y += dy
			// prevent index overflows
			if x < 0 || y < 0 || x >= len(seats[0]) || y >= len(seats) {
				// if x < 0 || y < 0 || x >= len(seats) || y >= len(seats[0]) {
				break
			}
			if string(seats[y][x]) == "L" {
				break
			}
			if string(seats[y][x]) == "#" {
				occupied++
				break
			}
		}
	}
	return occupied
}
