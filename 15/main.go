package main

import (
	"fmt"
	"time"
)

var (
	history []int
	next    int
)

// var history map[int]int

type intSlice []int

func main() {
	input := intSlice{1, 2, 16, 19, 18, 0}

	fmt.Println("Problem 1:", solve1(input, 2020)) // Output: 536
	t1 := time.Now()
	fmt.Println("Problem 2:", solve1(input, 30_000_000)) // Output: 24065124
	fmt.Println("Solved in:", time.Now().Sub(t1))
}

/*
	Problem 1:
	If that was the first time the number has been spoken, the current player says 0.
	Otherwise, the number had been spoken before; the current player announces how many turns apart the number is from when it was previously spoken.

	Given your starting numbers, what will be the 2020th number spoken?

	Given the starting numbers 1,3,2, the 2020th number spoken is 1.
	Given the starting numbers 2,1,3, the 2020th number spoken is 10.
	Given the starting numbers 1,2,3, the 2020th number spoken is 27.
	Given the starting numbers 2,3,1, the 2020th number spoken is 78.
	Given the starting numbers 3,2,1, the 2020th number spoken is 438.
	Given the starting numbers 3,1,2, the 2020th number spoken is 1836.

	Problem 2:
	Given your starting numbers, what will be the 30000000th number spoken?
	Given 0,3,6, the 30000000th number spoken is 175594.
	Given 1,3,2, the 30000000th number spoken is 2578.
	Given 2,1,3, the 30000000th number spoken is 3544142.
	Given 1,2,3, the 30000000th number spoken is 261214.
	Given 2,3,1, the 30000000th number spoken is 6895259.
	Given 3,2,1, the 30000000th number spoken is 18.
	Given 3,1,2, the 30000000th number spoken is 362.
*/
func solve1(input intSlice, max int) int {
	history = make([]int, max)
	for idx, n := range input {
		if idx == len(input)-1 {
			break
		}
		history[n] = idx + 1
	}

	prev := input[len(input)-1]
	for i := len(input) - 1; i < max-1; i++ {
		next := history[input[i]]
		if next != 0 {
			next = i - history[input[i]] + 1
		}

		history[prev] = i + 1

		input = append(input, next)
		prev = next
	}
	return input[len(input)-1]
}
