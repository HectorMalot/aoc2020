package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2020/utils"
)

func main() {
	lines, _ := utils.ImportToStringSlice("input.txt")

	// Parse
	input := [][]string{}
	for _, line := range lines {
		lineInput := []string{}
		carry := ""
		for _, c := range line {
			if string(c) == "n" || string(c) == "s" {
				carry = string(c)
				continue
			}
			lineInput = append(lineInput, fmt.Sprintf("%s%s", carry, string(c)))
			carry = ""
		}
		input = append(input, lineInput)
	}

	// Walk!
	f := make(floor)
	for _, i := range input {
		if v, ok := f[walk(i)]; ok {
			f[walk(i)] = !v
			continue
		}
		f[walk(i)] = true
	}

	// Print result
	fmt.Println("Problem 1:", f.sum()) // 287 (test: 10)

	// Living floor!
	// 1) Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
	// 2) Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.
	for i := 0; i < 100; i++ {
		next := make(floor)
		for k, v := range f {
			if v && (f.countNeighbours(k) == 0 || f.countNeighbours(k) > 2) {
				next[k] = false
			}
			if !v && f.countNeighbours(k) == 2 {
				next[k] = true
			}
			for _, n := range k.neighbours() {
				if f.valueAt(n) && (f.countNeighbours(n) == 0 || f.countNeighbours(n) > 2) {
					next[n] = false
				}
				if !f.valueAt(n) && f.countNeighbours(n) == 2 {
					next[n] = true
				}
			}
			if _, ok := next[k]; !ok {
				next[k] = f[k]
			}
		}
		f = next
		fmt.Println(i, f.sum()) // 15, 12, 25
	}
}

type point struct {
	x, y int
}

type floor map[point]bool

func walk(steps []string) point {
	pos := point{0, 0}
	for _, s := range steps {
		pos = pos.Add(step(s))
	}
	return pos
}

func step(s string) point {
	op := point{0, 0}
	switch s {
	case "e":
		op = point{2, 0}
	case "w":
		op = point{-2, 0}
	case "ne":
		op = point{1, 1}
	case "se":
		op = point{1, -1}
	case "nw":
		op = point{-1, 1}
	case "sw":
		op = point{-1, -1}
	}
	return op
}

func (p point) Add(p2 point) point {
	p.x += p2.x
	p.y += p2.y
	return p
}

func (p point) neighbours() []point {
	area := []point{
		{2, 0}, {-2, 0}, {-1, 1}, {-1, -1}, {1, -1}, {1, 1},
	}
	res := []point{}
	for _, d := range area {
		res = append(res, d.Add(p))
	}
	return res
}

// returns number of black tiles
func (f floor) countNeighbours(p point) int {
	sum := 0
	for _, n := range p.neighbours() {
		if f.valueAt(n) {
			sum++
		}
	}
	return sum
}

// true if black
func (f floor) valueAt(p point) bool {
	if v, ok := f[p]; ok {
		return v
	}
	return false
}

func (f floor) sum() int {
	sum := 0
	for _, v := range f {
		if v {
			sum++
		}
	}
	return sum
}
