module gitlab.com/hectormalot/aoc2020

go 1.15

require (
	github.com/deckarep/golang-set v1.7.1
	gonum.org/v1/plot v0.8.1 
)
