package main

import (
	"fmt"
	"strings"

	"gitlab.com/hectormalot/aoc2020/utils"
)

type game struct {
	// cups    []*cup
	cups    map[int]*cup
	current *cup
}

type cup struct {
	prev  *cup
	next  *cup
	value int
}

func main() {
	input := "952438716"

	// Parse input
	stringInput := strings.Split(input, "")

	g := newGame(stringInput)
	for n := 0; n < 100; n++ {
		g.next()
	}
	fmt.Println("Problem 1:", g.answer())

	// Problem 2
	g2 := newGame(stringInput)
	g2.crabify()

	for n := 0; n < 10_000_000; n++ {
		g2.next()
	}

	fmt.Println("Problem 2:", g2.answer2())

}

func (g *game) next() {
	// pick 3 clockwise cups
	pickedUp := []*cup{g.current.next, g.current.next.next, g.current.next.next.next}

	// close the loop again
	g.current.next = g.current.next.next.next.next
	g.current.next.next.next.next.prev = g.current

	// Determine the next value (i.e. destination cup)
	nextValue := g.current.value - 1
	for i := 0; i < 3; i++ {
		if nextValue == 0 {
			nextValue = len(g.cups) // Wrap around
		}
		// another -1 if it is in the pickup cups
		for _, c := range pickedUp {
			if nextValue == c.value {
				nextValue--
				continue
			}
		}
	}

	// Find the destination cup
	dCup := g.cups[nextValue]

	// move 3 cups to rhs of destination cup
	dCup.next, dCup.next.prev, pickedUp[0].prev, pickedUp[2].next = pickedUp[0], pickedUp[2], dCup, dCup.next

	// move to next cup
	g.current = g.current.next
}

func (g *game) string() string {
	str := ""
	for i := 0; i < 9; i++ {
		v := g.current.Next(i).value
		if g.current.value == v {
			str = fmt.Sprintf("%s(%d), ", str, v)
			continue
		}
		str = fmt.Sprintf("%s%d, ", str, v)
	}
	return str
}

func (g *game) answer() string {
	var c1 *cup
	for _, c := range g.cups {
		if c.value == 1 {
			c1 = c
			break
		}
	}
	res := ""
	for i := 1; i < 9; i++ {
		res = fmt.Sprintf("%s%d", res, c1.Next(i).value)
	}
	return res
}

func (g *game) answer2() string {
	return fmt.Sprintf("%d", g.cups[1].next.value*g.cups[1].Next(2).value)
}

func newGame(numbers []string) *game {
	// cups := []*cup{}
	cups := make(map[int]*cup, 10)
	var prev, c, c1 *cup
	for i, s := range numbers {
		c = &cup{prev: prev, next: nil, value: utils.MustAtoi(s)}
		if i == 0 {
			c1 = c
		}
		cups[c.value] = c
		if prev != nil {
			prev.next = c
		}
		prev = c
	}
	c1.prev = c
	c.next = c1
	return &game{cups: cups, current: c1}
}

func (g *game) crabify() {
	// Add up to a million cups!
	prev := g.current.prev

	// Create a new slice to hold the cups
	cups := make(map[int]*cup, 1_000_000)
	for k, c := range g.cups {
		cups[k] = c
	}
	g.cups = cups

	// add the cups
	var c *cup
	for i := 10; i <= 1_000_000; i++ {
		c = &cup{prev: prev, next: nil, value: i}
		g.cups[c.value] = c
		c.prev.next = c
		prev = c
	}

	// Fix the wrap
	g.current.prev = c
	c.next = g.current
}

func (c *cup) Next(i int) *cup {
	n := c
	for ; i > 0; i-- {
		n = n.next
	}
	return n
}

func (c *cup) Prev(i int) *cup {
	n := c
	for ; i > 0; i-- {
		n = n.prev
	}
	return n
}
