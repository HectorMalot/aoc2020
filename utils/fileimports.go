package utils

import (
	"bufio"
	"os"
	"strconv"
)

// ImportToStringSlice opens a whole file into memory and returns the content
// as a slice of strings
func ImportToStringSlice(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

// IntSlice is a slice of int
type IntSlice []int

// ImportToIntSlice opens a whole file into memory and returns the content
// as a slice of ints
func ImportToIntSlice(path string) ([]int, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		i, _ := strconv.Atoi(scanner.Text())
		lines = append(lines, i)
	}
	return lines, scanner.Err()
}

// Min returns the lowest value
func Min(is []int) int {
	min := is[0]
	for _, i := range is {
		if i < min {
			min = i
		}
	}
	return min
}

// Max returns the highest value
func Max(is []int) int {
	max := is[0]
	for _, i := range is {
		if i > max {
			max = i
		}
	}
	return max
}

// MustAtoi Converts a string to an int, panics otherwise
func MustAtoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return i
}
