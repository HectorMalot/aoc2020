package main

import (
	"fmt"
	"regexp"
	"strconv"

	"gitlab.com/hectormalot/aoc2020/utils"
)

type pwline struct {
	pattern string
	min     int
	max     int
	pass    string
}

func main() {
	problem1()
}

/*
Your flight departs in a few days from the coastal airport; the easiest way down to the coast from here is via toboggan.

The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day. "Something's wrong with our computers; we can't log in!" You ask if you can take a look.

Their password database seems to be a little corrupted: some of the passwords wouldn't have been allowed by the Official Toboggan Corporate Policy that was in effect when they were chosen.

To try to debug the problem, they have created a list (your puzzle input) of passwords (according to the corrupted database) and the corporate policy when that password was set.

For example, suppose you have the following list:

1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
Each line gives the password policy and then the password. The password policy indicates the lowest and highest number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password must contain a at least 1 time and at most 3 times.

In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no instances of b, but needs at least 1. The first and third passwords are valid: they contain one a or nine c, both within the limits of their respective policies.

How many passwords are valid according to their policies?
*/
func problem1() {
	// Import the dataset
	lines, _ := utils.ImportToStringSlice("input1.txt")

	// Parse the lines
	policies := []pwline{}
	for _, line := range lines {
		policies = append(policies, lineToPolicy(line))
	}

	// Count the valids
	count := 0
	for _, p := range policies {
		if p.validate() {
			count++
		}
	}
	fmt.Printf("Valid passwords for solution 1: %d\n", count)

	count = 0
	for _, p := range policies {
		if p.validateToboggan() {
			count++
		}
	}
	fmt.Printf("Valid passwords for solution 2: %d\n", count)

}

func (p *pwline) validate() bool {
	count := 0
	for _, c := range p.pass {
		if string(c) == p.pattern {
			count++
		}
	}
	if count < p.min || count > p.max {
		return false
	}
	return true
}

func lineToPolicy(line string) pwline {
	// 2-8 m: ttzhlrrmbt
	r := regexp.MustCompile(`(\d+)-(\d+) (\w+): (\w+)`)
	matches := r.FindStringSubmatch(line)
	min, _ := strconv.Atoi(matches[1])
	max, _ := strconv.Atoi(matches[2])
	return pwline{
		min:     min,
		max:     max,
		pattern: matches[3],
		pass:    matches[4],
	}
}

/*
While it appears you validated the passwords correctly, they don't seem to be what the Official Toboggan Corporate Authentication System is expecting.

The shopkeeper suddenly realizes that he just accidentally explained the password policy rules from his old job at the sled rental place down the street! The Official Toboggan Corporate Policy actually works a little differently.

Each policy actually describes two positions in the password, where 1 means the first character, 2 means the second character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) Exactly one of these positions must contain the given letter. Other occurrences of the letter are irrelevant for the purposes of policy enforcement.

Given the same example list from above:

1-3 a: abcde is valid: position 1 contains a and position 3 does not.
1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.
How many passwords are valid according to the new interpretation of the policies?
*/

func (p *pwline) validateToboggan() bool {
	fmt.Println(p.pass)
	if len(p.pass) < p.min {
		return false
	}
	if len(p.pass) < p.max {
		if string(p.pass[p.min+1]) == p.pattern {
			return true
		}
		return false
	}
	if string(p.pass[p.min-1]) == p.pattern && string(p.pass[p.max-1]) == p.pattern {
		return false
	}
	if string(p.pass[p.min-1]) != p.pattern && string(p.pass[p.max-1]) != p.pattern {
		return false
	}
	return true
}
