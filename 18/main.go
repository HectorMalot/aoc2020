package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/hectormalot/aoc2020/utils"
)

func main() {
	lines, _ := utils.ImportToStringSlice("input.txt")

	sum := 0
	for _, line := range lines {
		sum += token(line).eval()
	}
	fmt.Println("Problem 1:", sum)

	sum = 0
	for _, line := range lines {
		sum += token(line).eval2()
	}
	fmt.Println("Problem 2:", sum)
}

type token string

func (t token) eval() int {
	// 1. recursively replace every matching group with its answers
	for {
		if idx := strings.Index(string(t), "("); idx == -1 {
			break
		}
		t = replaceGroups(t, 1)
	}

	// Only simple calc remains here
	ops := strings.Split(string(t), " ")
	action := "+"
	acc := 0
	for _, op := range ops {
		switch op {
		case "+":
			action = "+"
		case "*":
			action = "*"
		default:
			// number
			n := utils.MustAtoi(op)
			if action == "+" {
				acc += n
			}
			if action == "*" {
				acc *= n
			}
		}
	}
	return acc
}

func (t token) eval2() int {
	// 1. recursively replace every matching group with its answers
	for {
		if idx := strings.Index(string(t), "("); idx == -1 {
			break
		}
		t = replaceGroups(t, 2)
	}

	// split by "*" to calc plusses, and multiply the results
	sums := strings.Split(string(t), " * ")
	acc := 1
	for _, s := range sums {
		acc *= token(s).eval()
	}

	return acc
}

func replaceGroups(t token, method int) token {
	// find matching parenthesis and replace with result
	idx := strings.Index(string(t), "(")
	n := 1
	for i := idx + 1; i < len(t); i++ {

		if string(t[i]) == ")" && n == 1 {
			res := ""
			if method == 1 {
				res = strconv.FormatInt(int64(t[idx+1:i].eval()), 10)
			} else {
				res = strconv.FormatInt(int64(t[idx+1:i].eval2()), 10)
			}
			return token(fmt.Sprintf("%s%s%s", t[:idx], res, t[i+1:]))
		}
		if string(t[i]) == "(" {
			n++
		}
		if string(t[i]) == ")" {
			n--
		}
	}
	return ""
}
